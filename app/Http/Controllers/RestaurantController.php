<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ZipController;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Zip;
use DB;//

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //DB is a allias of a facade Class so when you are using it first import it using (use Keyword). done. :)
        $zips = DB::table('zipcode')->get();

        //dd($zips); 3 ways to send data from controller to view;
      
        return view('dashboard.restaurant.add_restaurant', ['zips' => $zips]);
        
        //return view('dashboard.restaurant.add_restaurant')->with(compact('zips'));
        //return view('dashboard.restaurant.add_restaurant')->with('zips', $zips);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurant $restaurant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restaurant $restaurant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        //
    }

    public function myformAjax($id)
    {
        //$area = DB::table('zipcode')->where('id', $id)->get();
        $area = Zip::findOrFail($id);
        // dd($area);
        return json_encode($area);
    }
}
