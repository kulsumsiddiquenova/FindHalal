<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('restaurant_name');
            $table->string('address');
            $table->integer('zip');
            $table->string('area');
            $table->double('min_price', 4, 2);
            $table->double('delivery_charge', 4, 2);
            $table->timestamp('opening_hr')->nullable();
            $table->timestamp('closing_hr')->nullable();
            $table->double('distance', 4, 2);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant');
    }
}
