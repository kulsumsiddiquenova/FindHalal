<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title>Findhalal | Horizontal Menu Boxed</title>

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">

	<script src="assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container horizontal-menu">

@include('dashboard.sidebar')

<div class="main-content">

	<div class="row">

		<!-- Profile Info and Notifications -->
		<div class="col-md-6 col-sm-8 clearfix">

			<ul class="user-info pull-left pull-none-xsm">

				<!-- Profile Info -->
				<li class="profile-info dropdown">
					<!-- add class "pull-right" if you want to place this from right -->

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<h2>Findhalal </h2>
					</a>

					<ul class="dropdown-menu">

						<!-- Reverse Caret -->
						<li class="caret"></li>

						<!-- Profile sub-links -->
						<li>
							<a href="extra-timeline.html">
							<i class="entypo-user"></i>
							Edit Profile
						</a>
						</li>

						<li>
							<a href="mailbox.html">
							<i class="entypo-mail"></i>
							Inbox
						</a>
						</li>

						<li>
							<a href="extra-calendar.html">
							<i class="entypo-calendar"></i>
							Calendar
						</a>
						</li>

						<li>
							<a href="#">
							<i class="entypo-clipboard"></i>
							Tasks
						</a>
						</li>
					</ul>
				</li>

			</ul>

			<ul class="user-info pull-left pull-right-xs pull-none-xsm">

				<!-- Raw Notifications -->
				<li class="notifications dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-attention"></i>
					<span class="badge badge-info">6</span>
				</a>

					<ul class="dropdown-menu">
						<li class="top">
							<p class="small">
								<a href="#" class="pull-right">Mark all Read</a> You have <strong>3</strong> new notifications.
							</p>
						</li>

						<li>
							<ul class="dropdown-menu-list scroller">
								<li class="unread notification-success">
									<a href="#">
									<i class="entypo-user-add pull-right"></i>
									
									<span class="line">
										<strong>New user registered</strong>
									</span>
									
									<span class="line small">
										30 seconds ago
									</span>
								</a>
								</li>

								<li class="unread notification-secondary">
									<a href="#">
									<i class="entypo-heart pull-right"></i>
									
									<span class="line">
										<strong>Someone special liked this</strong>
									</span>
									
									<span class="line small">
										2 minutes ago
									</span>
								</a>
								</li>

								<li class="notification-primary">
									<a href="#">
									<i class="entypo-user pull-right"></i>
									
									<span class="line">
										<strong>Privacy settings have been changed</strong>
									</span>
									
									<span class="line small">
										3 hours ago
									</span>
								</a>
								</li>

								<li class="notification-danger">
									<a href="#">
									<i class="entypo-cancel-circled pull-right"></i>
									
									<span class="line">
										John cancelled the event
									</span>
									
									<span class="line small">
										9 hours ago
									</span>
								</a>
								</li>

								<li class="notification-info">
									<a href="#">
									<i class="entypo-info pull-right"></i>
									
									<span class="line">
										The server is status is stable
									</span>
									
									<span class="line small">
										yesterday at 10:30am
									</span>
								</a>
								</li>

								<li class="notification-warning">
									<a href="#">
									<i class="entypo-rss pull-right"></i>
									
									<span class="line">
										New comments waiting approval
									</span>
									
									<span class="line small">
										last week
									</span>
								</a>
								</li>
							</ul>
						</li>

						<li class="external">
							<a href="#">View all notifications</a>
						</li>
					</ul>

				</li>

				<!-- Message Notifications -->
				<li class="notifications dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-mail"></i>
					<span class="badge badge-secondary">10</span>
				</a>

					<ul class="dropdown-menu">
						<li>
							<form class="top-dropdown-search">

								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search anything..." name="s" />
								</div>

							</form>

							<ul class="dropdown-menu-list scroller">
								<li class="active">
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-1@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										<strong>Luc Chartier</strong>
										- yesterday
									</span>
									
									<span class="line desc small">
										This ain’t our first item, it is the best of the rest.
									</span>
								</a>
								</li>

								<li class="active">
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-2@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										<strong>Salma Nyberg</strong>
										- 2 days ago
									</span>
									
									<span class="line desc small">
										Oh he decisively impression attachment friendship so if everything. 
									</span>
								</a>
								</li>

								<li>
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-3@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										Hayden Cartwright
										- a week ago
									</span>
									
									<span class="line desc small">
										Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
									</span>
								</a>
								</li>

								<li>
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-4@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										Sandra Eberhardt
										- 16 days ago
									</span>
									
									<span class="line desc small">
										On so attention necessary at by provision otherwise existence direction.
									</span>
								</a>
								</li>
							</ul>
						</li>

						<li class="external">
							<a href="mailbox.html">All Messages</a>
						</li>
					</ul>

				</li>

				<!-- Task Notifications -->
				<li class="notifications dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-list"></i>
					<span class="badge badge-warning">1</span>
				</a>

					<ul class="dropdown-menu">
						<li class="top">
							<p>You have 6 pending tasks</p>
						</li>

						<li>
							<ul class="dropdown-menu-list scroller">
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Procurement</span>
										<span class="percent">27%</span>
									</span>
								
									<span class="progress">
										<span style="width: 27%;" class="progress-bar progress-bar-success">
											<span class="sr-only">27% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">App Development</span>
										<span class="percent">83%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 83%;" class="progress-bar progress-bar-danger">
											<span class="sr-only">83% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">HTML Slicing</span>
										<span class="percent">91%</span>
									</span>
									
									<span class="progress">
										<span style="width: 91%;" class="progress-bar progress-bar-success">
											<span class="sr-only">91% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Database Repair</span>
										<span class="percent">12%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 12%;" class="progress-bar progress-bar-warning">
											<span class="sr-only">12% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Backup Create Progress</span>
										<span class="percent">54%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 54%;" class="progress-bar progress-bar-info">
											<span class="sr-only">54% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Upgrade Progress</span>
										<span class="percent">17%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 17%;" class="progress-bar progress-bar-important">
											<span class="sr-only">17% Complete</span>
										</span>
									</span>
								</a>
								</li>
							</ul>
						</li>

						<li class="external">
							<a href="#">See all tasks</a>
						</li>
					</ul>

				</li>

			</ul>

		</div>



		<!-- Raw Links -->
		<div class="col-md-6 col-sm-4 clearfix hidden-xs">
	
	<ul class="list-inline links-list pull-right">



		<li class="sep"></li>

		<li>
			<a href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				Log Out <i class="entypo-logout right"></i>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
			 </form>
		</li>
	</ul>

</div>

</div>

<hr />

<script type="text/javascript">
function showAjaxModal()
{
	jQuery('#modal-7').modal('show', {backdrop: 'static'});
	
	jQuery.ajax({
		url: "data/ajax-content.txt",
		success: function(response)
		{
			jQuery('#modal-7 .modal-body').html(response);
		}
	});
}
</script><!-- Footer -->
<footer class="main">
	
	&copy; 2018 <strong> <a href="http://findhalal.de" target="_blank">Findhalal.de</a>

</footer>
						</div>
			</div>
		</div>
			</div>

	
<div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">

	<div class="chat-inner">


		<h2 class="chat-header">
			<a href="#" class="chat-close"><i class="entypo-cancel"></i></a>

			<i class="entypo-users"></i>
			Chat
			<span class="badge badge-success is-hidden">0</span>
		</h2>


		<div class="chat-group" id="group-1">
			<strong>Favorites</strong>

			<a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
			<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
			<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
			<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
			<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
		</div>


		<div class="chat-group" id="group-2">
			<strong>Work</strong>

			<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
			<a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
			<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
		</div>


		<div class="chat-group" id="group-3">
			<strong>Social</strong>

			<a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
			<a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
			<a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
			<a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
		</div>

	</div>

	<!-- conversation template -->
	<div class="chat-conversation">

		<div class="conversation-header">
			<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>

			<span class="user-status"></span>
			<span class="display-name"></span>
			<small></small>
		</div>

		<ul class="conversation-body">
		</ul>

		<div class="chat-textarea">
			<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
		</div>

	</div>

</div>


<!-- Chat Histories -->
<ul class="chat-history" id="sample_history">
	<li>
		<span class="user">Art Ramadani</span>
		<p>Are you here?</p>
		<span class="time">09:00</span>
	</li>

	<li class="opponent">
		<span class="user">Catherine J. Watkins</span>
		<p>This message is pre-queued.</p>
		<span class="time">09:25</span>
	</li>

	<li class="opponent">
		<span class="user">Catherine J. Watkins</span>
		<p>Whohoo!</p>
		<span class="time">09:26</span>
	</li>

	<li class="opponent unread">
		<span class="user">Catherine J. Watkins</span>
		<p>Do you like it?</p>
		<span class="time">09:27</span>
	</li>
</ul>




<!-- Chat Histories -->
<ul class="chat-history" id="sample_history_2">
	<li class="opponent unread">
		<span class="user">Daniel A. Pena</span>
		<p>I am going out.</p>
		<span class="time">08:21</span>
	</li>

	<li class="opponent unread">
		<span class="user">Daniel A. Pena</span>
		<p>Call me when you see this message.</p>
		<span class="time">08:27</span>
	</li>
</ul></div>


	<!-- Sample Modal (Default skin) -->
	<div class="modal fade" id="sample-modal-dialog-1">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Default Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Sample Modal (Skin inverted) -->
	<div class="modal invert fade" id="sample-modal-dialog-2">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Inverted Skin Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Sample Modal (Skin gray) -->
	<div class="modal gray fade" id="sample-modal-dialog-3">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Gray Skin Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal 1 (Basic)-->
	<div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Basic Modal</h4>
				</div>
				
				<div class="modal-body">
					Hello I am a Modal!
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal 2 (Custom Width)-->
	<div class="modal fade custom-width" id="modal-2">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Custom Width Modal</h4>
				</div>
				
				<div class="modal-body">
					Any type of width can be applied.
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal 3 (Custom Width)-->
	<div class="modal fade custom-width" id="modal-3">
		<div class="modal-dialog" style="width: 96%">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Full width</h4>
				</div>
				
				<div class="modal-body">
				
					Its about 100%
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- Modal 4 (Confirm)-->
	<div class="modal fade" id="modal-4" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<h4 class="modal-title">Confirm Modal</h4>
				</div>
				
				<div class="modal-body">
				
					You can close this modal when you click on button only!
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Continue</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Modal 5 (Long Modal)-->
	<div class="modal fade" id="modal-5">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Long Height Modal</h4>
				</div>
				
				<div class="modal-body">
				
					<img src="assets/images/burjkhalifa.jpg" alt="" class="img-responsive" />
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="modal-6">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal Content is Responsive</h4>
				</div>
				
				<div class="modal-body">
				
					<div class="row">
						<div class="col-md-6">
							
							<div class="form-group">
								<label for="field-1" class="control-label">Name</label>
								
								<input type="text" class="form-control" id="field-1" placeholder="John">
							</div>	
							
						</div>
						
						<div class="col-md-6">
							
							<div class="form-group">
								<label for="field-2" class="control-label">Surname</label>
								
								<input type="text" class="form-control" id="field-2" placeholder="Doe">
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-3" class="control-label">Address</label>
								
								<input type="text" class="form-control" id="field-3" placeholder="Address">
							</div>	
							
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-4">
							
							<div class="form-group">
								<label for="field-4" class="control-label">City</label>
								
								<input type="text" class="form-control" id="field-4" placeholder="Boston">
							</div>	
							
						</div>
						
						<div class="col-md-4">
							
							<div class="form-group">
								<label for="field-5" class="control-label">Country</label>
								
								<input type="text" class="form-control" id="field-5" placeholder="United States">
							</div>	
						
						</div>
						
						<div class="col-md-4">
							
							<div class="form-group">
								<label for="field-6" class="control-label">Zip</label>
								
								<input type="text" class="form-control" id="field-6" placeholder="123456">
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group no-margin">
								<label for="field-7" class="control-label">Personal Info</label>
								
								<textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself"></textarea>
							</div>	
							
						</div>
					</div>
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal 7 (Ajax Modal)-->
	<div class="modal fade" id="modal-7">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Dynamic Content</h4>
				</div>
				
				<div class="modal-body">
				
					Content is loading...
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>



	<!-- Bottom scripts (common) -->
	<script src="assets/js/gsap/TweenMax.min.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>


	<!-- Imported scripts on this page -->
	<script src="assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>