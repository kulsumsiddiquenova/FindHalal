<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title>Findhalal | Mailbox</title>

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">

	<script src="assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	

	@include('dashboard.sidebar')

	<div class="main-content">

		<div class="row">

			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">

				<ul class="user-info pull-left pull-none-xsm">

					<!-- Profile Info -->
					<li class="profile-info dropdown">
						<!-- add class "pull-right" if you want to place this from right -->

						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<h2>Findhalal </h2>
						</a>

						<ul class="dropdown-menu">

							<!-- Reverse Caret -->
							<li class="caret"></li>

							<!-- Profile sub-links -->
							<li>
								<a href="extra-timeline.html">
								<i class="entypo-user"></i>
								Edit Profile
							</a>
							</li>

							<li>
								<a href="mailbox.html">
								<i class="entypo-mail"></i>
								Inbox
							</a>
							</li>

							<li>
								<a href="extra-calendar.html">
								<i class="entypo-calendar"></i>
								Calendar
							</a>
							</li>

							<li>
								<a href="#">
								<i class="entypo-clipboard"></i>
								Tasks
							</a>
							</li>
						</ul>
					</li>

				</ul>

				<ul class="user-info pull-left pull-right-xs pull-none-xsm">

					<!-- Raw Notifications -->
					<li class="notifications dropdown">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="entypo-attention"></i>
						<span class="badge badge-info">6</span>
					</a>

						<ul class="dropdown-menu">
							<li class="top">
								<p class="small">
									<a href="#" class="pull-right">Mark all Read</a> You have <strong>3</strong> new notifications.
								</p>
							</li>

							<li>
								<ul class="dropdown-menu-list scroller">
									<li class="unread notification-success">
										<a href="#">
										<i class="entypo-user-add pull-right"></i>
										
										<span class="line">
											<strong>New user registered</strong>
										</span>
										
										<span class="line small">
											30 seconds ago
										</span>
									</a>
									</li>

									<li class="unread notification-secondary">
										<a href="#">
										<i class="entypo-heart pull-right"></i>
										
										<span class="line">
											<strong>Someone special liked this</strong>
										</span>
										
										<span class="line small">
											2 minutes ago
										</span>
									</a>
									</li>

									<li class="notification-primary">
										<a href="#">
										<i class="entypo-user pull-right"></i>
										
										<span class="line">
											<strong>Privacy settings have been changed</strong>
										</span>
										
										<span class="line small">
											3 hours ago
										</span>
									</a>
									</li>

									<li class="notification-danger">
										<a href="#">
										<i class="entypo-cancel-circled pull-right"></i>
										
										<span class="line">
											John cancelled the event
										</span>
										
										<span class="line small">
											9 hours ago
										</span>
									</a>
									</li>

									<li class="notification-info">
										<a href="#">
										<i class="entypo-info pull-right"></i>
										
										<span class="line">
											The server is status is stable
										</span>
										
										<span class="line small">
											yesterday at 10:30am
										</span>
									</a>
									</li>

									<li class="notification-warning">
										<a href="#">
										<i class="entypo-rss pull-right"></i>
										
										<span class="line">
											New comments waiting approval
										</span>
										
										<span class="line small">
											last week
										</span>
									</a>
									</li>
								</ul>
							</li>

							<li class="external">
								<a href="#">View all notifications</a>
							</li>
						</ul>

					</li>

					<!-- Message Notifications -->
					<li class="notifications dropdown">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="entypo-mail"></i>
						<span class="badge badge-secondary">10</span>
					</a>

						<ul class="dropdown-menu">
							<li>
								<form class="top-dropdown-search">

									<div class="form-group">
										<input type="text" class="form-control" placeholder="Search anything..." name="s" />
									</div>

								</form>

								<ul class="dropdown-menu-list scroller">
									<li class="active">
										<a href="#">
										<span class="image pull-right">
											<img src="assets/images/thumb-1@2x.png" width="44" alt="" class="img-circle" />
										</span>
										
										<span class="line">
											<strong>Luc Chartier</strong>
											- yesterday
										</span>
										
										<span class="line desc small">
											This ain’t our first item, it is the best of the rest.
										</span>
									</a>
									</li>

									<li class="active">
										<a href="#">
										<span class="image pull-right">
											<img src="assets/images/thumb-2@2x.png" width="44" alt="" class="img-circle" />
										</span>
										
										<span class="line">
											<strong>Salma Nyberg</strong>
											- 2 days ago
										</span>
										
										<span class="line desc small">
											Oh he decisively impression attachment friendship so if everything. 
										</span>
									</a>
									</li>

									<li>
										<a href="#">
										<span class="image pull-right">
											<img src="assets/images/thumb-3@2x.png" width="44" alt="" class="img-circle" />
										</span>
										
										<span class="line">
											Hayden Cartwright
											- a week ago
										</span>
										
										<span class="line desc small">
											Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
										</span>
									</a>
									</li>

									<li>
										<a href="#">
										<span class="image pull-right">
											<img src="assets/images/thumb-4@2x.png" width="44" alt="" class="img-circle" />
										</span>
										
										<span class="line">
											Sandra Eberhardt
											- 16 days ago
										</span>
										
										<span class="line desc small">
											On so attention necessary at by provision otherwise existence direction.
										</span>
									</a>
									</li>
								</ul>
							</li>

							<li class="external">
								<a href="mailbox.html">All Messages</a>
							</li>
						</ul>

					</li>

					<!-- Task Notifications -->
					<li class="notifications dropdown">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="entypo-list"></i>
						<span class="badge badge-warning">1</span>
					</a>

						<ul class="dropdown-menu">
							<li class="top">
								<p>You have 6 pending tasks</p>
							</li>

							<li>
								<ul class="dropdown-menu-list scroller">
									<li>
										<a href="#">
										<span class="task">
											<span class="desc">Procurement</span>
											<span class="percent">27%</span>
										</span>
									
										<span class="progress">
											<span style="width: 27%;" class="progress-bar progress-bar-success">
												<span class="sr-only">27% Complete</span>
											</span>
										</span>
									</a>
									</li>
									<li>
										<a href="#">
										<span class="task">
											<span class="desc">App Development</span>
											<span class="percent">83%</span>
										</span>
										
										<span class="progress progress-striped">
											<span style="width: 83%;" class="progress-bar progress-bar-danger">
												<span class="sr-only">83% Complete</span>
											</span>
										</span>
									</a>
									</li>
									<li>
										<a href="#">
										<span class="task">
											<span class="desc">HTML Slicing</span>
											<span class="percent">91%</span>
										</span>
										
										<span class="progress">
											<span style="width: 91%;" class="progress-bar progress-bar-success">
												<span class="sr-only">91% Complete</span>
											</span>
										</span>
									</a>
									</li>
									<li>
										<a href="#">
										<span class="task">
											<span class="desc">Database Repair</span>
											<span class="percent">12%</span>
										</span>
										
										<span class="progress progress-striped">
											<span style="width: 12%;" class="progress-bar progress-bar-warning">
												<span class="sr-only">12% Complete</span>
											</span>
										</span>
									</a>
									</li>
									<li>
										<a href="#">
										<span class="task">
											<span class="desc">Backup Create Progress</span>
											<span class="percent">54%</span>
										</span>
										
										<span class="progress progress-striped">
											<span style="width: 54%;" class="progress-bar progress-bar-info">
												<span class="sr-only">54% Complete</span>
											</span>
										</span>
									</a>
									</li>
									<li>
										<a href="#">
										<span class="task">
											<span class="desc">Upgrade Progress</span>
											<span class="percent">17%</span>
										</span>
										
										<span class="progress progress-striped">
											<span style="width: 17%;" class="progress-bar progress-bar-important">
												<span class="sr-only">17% Complete</span>
											</span>
										</span>
									</a>
									</li>
								</ul>
							</li>

							<li class="external">
								<a href="#">See all tasks</a>
							</li>
						</ul>

					</li>

				</ul>

			</div>

		<!-- Raw Links -->
		<div class="col-md-6 col-sm-4 clearfix hidden-xs">
	
	<ul class="list-inline links-list pull-right">



		<li class="sep"></li>

		<li>
			<a href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				Log Out <i class="entypo-logout right"></i>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
			 </form>
		</li>
	</ul>

</div>

</div>

<hr />
		
		
		<div class="mail-env">
		
			<!-- compose new email button -->
			<div class="mail-sidebar-row visible-xs">
				<a href="mailbox-compose.html" class="btn btn-success btn-icon btn-block">
					Compose Mail
					<i class="entypo-pencil"></i>
				</a>
			</div>
			
			
			<!-- Mail Body -->
			<div class="mail-body">
				
				<div class="mail-header">
					<!-- title -->
					<h3 class="mail-title">
						Inbox
						<span class="count">(6)</span>
					</h3>
					
					<!-- search -->
					<form method="get" role="form" class="mail-search">
						<div class="input-group">
							<input type="text" class="form-control" name="s" placeholder="Search for mail..." />
							
							<div class="input-group-addon">
								<i class="entypo-search"></i>
							</div>
						</div>
					</form>
				</div>
				
				
				<!-- mail table -->
				<table class="table mail-table">
					<!-- mail table header -->
					<thead>
						<tr>
							<th width="5%">
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</th>
							<th colspan="4">
								
								<div class="mail-select-options">Mark as Read</div>
								
								<div class="mail-pagination" colspan="2">
									<strong>1-30</strong> <span>of 789</span>
									
									<div class="btn-group">
										<a href="#" class="btn btn-sm btn-white"><i class="entypo-left-open"></i></a>
										<a href="#" class="btn btn-sm btn-white"><i class="entypo-right-open"></i></a>
									</div>
								</div>
							</th>
						</tr>
					</thead>
					
					<!-- email list -->
					<tbody>
						<tr class="unread"><!-- new email class: unread -->
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star stared">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Facebook</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Reset your account password
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">13:52</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Google AdWords</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Google AdWords: Ads not serving
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">09:27</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Apple.com</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									<span class="label label-danger">Business</span>
									Your apple account ID has been accessed from un-familiar location.
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">Today</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">World Weather Online</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Over Throttle Alert
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">Yesterday</td>
						</tr>
						
						<tr class="unread">
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Dropbox</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Complete your Dropbox setup!
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">4 Dec</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star stared">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Arlind Nushi</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									<span class="label label-warning">Friends</span>
									Work progress for Neon Project
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">28 Nov</td>
						</tr>
						
						<tr class="unread"><!-- new email class: unread -->
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star stared">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Jose D. Gardner</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Regarding to your website issues.
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">22 Nov</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Aurelio D. Cummins</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Steadicam operator
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">15 Nov</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Filan Fisteku</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									You are loosing clients because your website is not responsive.
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">02 Nov</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Instagram</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Instagram announces the new video uploadin feature.
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">26 Oct</td>
						</tr>
						
						<tr class="unread">
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">James Blue</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									<span class="label label-info">Sports</span>
									There are 20 notifications
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">18 Oct</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">SomeHost</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Bugs in your system.
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">01 Sep</td>
						</tr>
						
						<tr class="unread"><!-- new email class: unread -->
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star stared">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Facebook</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Reset your account password
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">13:52</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Google AdWords</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Google AdWords: Ads not serving
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">09:27</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Apple.com</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									<span class="label label-danger">Business</span>
									Your apple account ID has been accessed from un-familiar location.
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">Today</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">World Weather Online</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Over Throttle Alert
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">Yesterday</td>
						</tr>
						
						<tr class="unread">
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Dropbox</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Complete your Dropbox setup!
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">4 Dec</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star stared">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Arlind Nushi</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									<span class="label label-warning">Friends</span>
									Work progress for Neon Project
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">28 Nov</td>
						</tr>
						
						<tr class="unread"><!-- new email class: unread -->
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star stared">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Jose D. Gardner</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Regarding to your website issues.
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">22 Nov</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Aurelio D. Cummins</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Steadicam operator
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">15 Nov</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Filan Fisteku</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									You are loosing clients because your website is not responsive.
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">02 Nov</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">Instagram</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Instagram announces the new video uploadin feature.
								</a>
							</td>
							<td class="col-options">
								<a href="mailbox-message.html"><i class="entypo-attach"></i></a>
							</td>
							<td class="col-time">26 Oct</td>
						</tr>
						
						<tr class="unread">
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">James Blue</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									<span class="label label-info">Sports</span>
									There are 20 notifications
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">18 Oct</td>
						</tr>
						
						<tr>
							<td>
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</td>
							<td class="col-name">
								<a href="#" class="star">
									<i class="entypo-star"></i>
								</a>
								<a href="mailbox-message.html" class="col-name">SomeHost</a>
							</td>
							<td class="col-subject">
								<a href="mailbox-message.html">
									Bugs in your system.
								</a>
							</td>
							<td class="col-options"></td>
							<td class="col-time">01 Sep</td>
						</tr>
					</tbody>
					
					<!-- mail table footer -->
					<tfoot>
						<tr>
							<th width="5%">
								<div class="checkbox checkbox-replace">
									<input type="checkbox" />
								</div>
							</th>
							<th colspan="4">
								
								<div class="mail-pagination" colspan="2">
									<strong>1-30</strong> <span>of 789</span>
									
									<div class="btn-group">
										<a href="#" class="btn btn-sm btn-white"><i class="entypo-left-open"></i></a>
										<a href="#" class="btn btn-sm btn-white"><i class="entypo-right-open"></i></a>
									</div>
								</div>
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
			
			<!-- Sidebar -->
			<div class="mail-sidebar">
				
				<!-- compose new email button -->
				<div class="mail-sidebar-row hidden-xs">
					<a href="mailbox-compose.html" class="btn btn-success btn-icon btn-block">
						Compose Mail
						<i class="entypo-pencil"></i>
					</a>
				</div>
				
				<!-- menu -->
				<ul class="mail-menu">
					<li class="active">
						<a href="#">
							<span class="badge badge-danger pull-right">6</span>
							Inbox
						</a>
					</li>
					
					<li>
						<a href="#">
							<span class="badge badge-gray pull-right">1</span>
							Sent
						</a>
					</li>
					
					<li>
						<a href="#">
							Drafts
						</a>
					</li>
					
					<li>
						<a href="#">
							<span class="badge badge-gray pull-right">1</span>
							Spam
						</a>
					</li>
					
					<li>
						<a href="#">
							Trash
						</a>
					</li>
				</ul>
				
				<div class="mail-distancer"></div>
				
				<h4>Tags</h4>
				
				<!-- menu -->
				<ul class="mail-menu">
					<li>
						<a href="#">
							<span class="badge badge-danger badge-roundless pull-right"></span>
							Business
						</a>
					</li>
					
					<li>
						<a href="#">
							<span class="badge badge-info badge-roundless pull-right"></span>
							Sports
						</a>
					</li>
					
					<li>
						<a href="#">
							<span class="badge badge-warning badge-roundless pull-right"></span>
							Friends
						</a>
					</li>
				</ul>
				
			</div>
			
		</div>
		
		<hr />
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2018 <strong> <a href="http://findhalal.de" target="_blank">Findhalal.de</a>
		
		</footer>
	</div>

		
	<div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
	
		<div class="chat-inner">
	
	
			<h2 class="chat-header">
				<a href="#" class="chat-close"><i class="entypo-cancel"></i></a>
	
				<i class="entypo-users"></i>
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
	
	
			<div class="chat-group" id="group-1">
				<strong>Favorites</strong>
	
				<a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
			</div>
	
	
			<div class="chat-group" id="group-2">
				<strong>Work</strong>
	
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
			</div>
	
	
			<div class="chat-group" id="group-3">
				<strong>Social</strong>
	
				<a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
			</div>
	
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
	
			<div class="conversation-header">
				<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>
	
				<span class="user-status"></span>
				<span class="display-name"></span>
				<small></small>
			</div>
	
			<ul class="conversation-body">
			</ul>
	
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
	
		</div>
	
	</div>
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history">
		<li>
			<span class="user">Art Ramadani</span>
			<p>Are you here?</p>
			<span class="time">09:00</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>This message is pre-queued.</p>
			<span class="time">09:25</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>Whohoo!</p>
			<span class="time">09:26</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Catherine J. Watkins</span>
			<p>Do you like it?</p>
			<span class="time">09:27</span>
		</li>
	</ul>
	
	
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history_2">
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>I am going out.</p>
			<span class="time">08:21</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>Call me when you see this message.</p>
			<span class="time">08:27</span>
		</li>
	</ul>

	
</div>




	<!-- Bottom scripts (common) -->
	<script src="assets/js/gsap/TweenMax.min.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>


	<!-- Imported scripts on this page -->
	<script src="assets/js/neon-mail.js"></script>
	<script src="assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>